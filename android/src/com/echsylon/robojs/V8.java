package com.echsylon.robojs;

public final class V8 {

    private boolean isInitialized;

    static {
        System.loadLibrary("v8_engine");
    }

    private native void initializeContext();

    private native void disposeContext();

    private native boolean isContextInitialized();

    private native String executeScript(String javaScript);

    public void initialize() {
        initializeContext();
    }

    public void dispose() {
        disposeContext();
    }

    public String executeJavaScript(String script) {
        if (!isContextInitialized()) {
            initializeContext();
        }

        return executeScript(script);
    }
}
