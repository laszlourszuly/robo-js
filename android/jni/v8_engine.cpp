#include <jni.h>
#include <v8.h>

using namespace v8;

extern "C" {
	JNIEXPORT jstring Java_com_echsylon_robojs_V8_executeScript(JNIEnv*, jobject, jstring);
	JNIEXPORT jboolean Java_com_echsylon_robojs_V8_isContextInitialized(JNIEnv*, jobject);
	JNIEXPORT void Java_com_echsylon_robojs_V8_initializeContext(JNIEnv* , jobject);
	JNIEXPORT void Java_com_echsylon_robojs_V8_disposeContext(JNIEnv*, jobject);
}

Persistent<Context> m_context;
jboolean m_isInitialized = JNI_FALSE;

const char* toCString(const String::Utf8Value& value) {
  return *value ? *value : '\0';
}

const char* runJavaScript(const char* javaScript) {
	// Setup the V8 java script engine scope.
	HandleScope handleScope;
	Context::Scope context_scope(m_context);
	// Run the given java script.
	Handle<String> source = String::New(javaScript);
	Handle<Script> script = Script::Compile(source);
	Handle<Value> result = script->Run();;

	String::Utf8Value output(result);
	return toCString(output);
}

void disposeContext() {
	if (m_isInitialized == JNI_TRUE) {
		m_context.Dispose();
		m_isInitialized = JNI_FALSE;
	}
}

void initContext() {
	m_context = Context::New();
	m_isInitialized = JNI_TRUE;
}

void Java_com_echsylon_robojs_V8_initializeContext(JNIEnv* env, jobject javaThis) {
	initContext();
}

void Java_com_echsylon_robojs_V8_disposeContext(JNIEnv* env, jobject javaThis) {
	disposeContext();
}

jboolean Java_com_echsylon_robojs_V8_isContextInitialized(JNIEnv* env, jobject javaThis) {
	return m_isInitialized;
}

jstring Java_com_echsylon_robojs_V8_executeScript(JNIEnv* env, jobject javaThis, jstring javaScript) {
	// Make sure the script is an expected C-style char array.
	const char* script = env->GetStringUTFChars(javaScript, 0);
	// Run the given script in the V8 engine.
	const char* result = runJavaScript(script);
	// Release the C-style resources.
	env->ReleaseStringUTFChars(javaScript, script);

	return env->NewStringUTF(result);
}
