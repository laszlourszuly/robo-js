LOCAL_PATH := $(call my-dir)

# V8 Base
include $(CLEAR_VARS)
LOCAL_MODULE := v8_base
LOCAL_SRC_FILES := ../libs/armeabi/libv8_base.a
include $(PREBUILT_STATIC_LIBRARY)

# V8 Nosnapshot
include $(CLEAR_VARS)
LOCAL_MODULE := v8_snapshot
LOCAL_SRC_FILES := ../libs/armeabi/libv8_snapshot.a
include $(PREBUILT_STATIC_LIBRARY)

# Here we give our module name and source file(s)
include $(CLEAR_VARS)
LOCAL_MODULE := v8_engine
LOCAL_C_INCLUDES = ../v8/include
LOCAL_SRC_FILES := v8_engine.cpp
LOCAL_STATIC_LIBRARIES := v8_base v8_snapshot
include $(BUILD_SHARED_LIBRARY)