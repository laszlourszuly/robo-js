# Enable this when compiling the APK for the x86 platform
# (typically with a AVD with native platform support) but
# beware! This also requires the V8 libraries to be compiled
# for the x86 platform (runing on a real device would
# typically require ARM-compiled libraries).
#APP_ABI := x86
